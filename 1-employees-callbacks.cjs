/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

//1. Retrieve data for ids : [2, 13, 23].
const fs=require('fs');
const path=require('path');
//const JSONData=require('./data.json')
const getData=fs.readFile('./data.json','utf-8',((err,data)=>{
    if(err)
    {
        console.log(err)
    }
    else{
        const IDsData=JSON.parse(data).employees
        const getDataForIDs=IDsData.filter((ID)=>{
            return ID.id===2||ID.id===13||ID.id===23
        })
        //console.log(getDataForIDs)
        fs.writeFile('testProblem1.json',JSON.stringify(getDataForIDs),((err)=>{
            if(err)
            {
                console.log(err)
            }
            else{
        //         const clonedata=IDsData.map((item)=>{
        //             return {...item}
        //         })
        //         const groupCompnies=clonedata.reduce((acc,curr)=>{
        //             if(acc[curr.compnay])
        //             {
        //                 let comp={}
        //                 comp.id=curr.id
        //                 comp.name=curr.name
        //                 acc[curr.compnay].push(comp);
        //             }
        //             else{
        //                 //acc[curr.compnay]={};
        //                 let arrData=[]
        //                 let newComp={}
        //                 newComp.id=curr.id
        //                 newComp.name=curr.name
        //                 arrData.push(newComp)
        //                 acc[curr.compnay]=arrData;
        //             }
        //             return acc;
        //         },{})
        //         console.log(groupCompnies)
        //     }
        // }))
        // fs.readFile('./data.json','utf-8',((err,data)=>{
        //             if(err)
        //             {
        //                 console.log(err)
        //             }
                    // else{
                        const companyData=JSON.parse(data).employees
                        const getDataByCompanyName=companyData.filter((company)=>{
                            return company.company=='Powerpuff Brigade'
                        })
                        //console.log(getDataByCompanyName)
                        fs.writeFile('testProblem2.json',JSON.stringify(getDataByCompanyName),((err)=>{
                            if(err)
                            {
                                console.log(err)
                            }
                        }))
                    }
                }))
    }
}))


